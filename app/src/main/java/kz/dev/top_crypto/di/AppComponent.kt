package kz.dev.top_crypto.di

import dagger.Component
import kz.dev.top_crypto.MainActivity
import kz.dev.top_crypto.fragments.CurrenciesListFragment
import kz.dev.top_crypto.mvp.presenter.CurrenciesPresenter
import kz.dev.top_crypto.mvp.presenter.LatestChartPresenter
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, RestModule::class, MvpModule::class, ChartModule::class))
@Singleton
interface AppComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(presenter: CurrenciesPresenter)
    fun inject(presenter: LatestChartPresenter)

    fun inject(fragment: CurrenciesListFragment)
}

