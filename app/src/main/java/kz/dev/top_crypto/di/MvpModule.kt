package kz.dev.top_crypto.di
import dagger.Module
import dagger.Provides
import kz.dev.top_crypto.mvp.presenter.CurrenciesPresenter
import kz.dev.top_crypto.mvp.presenter.LatestChartPresenter
import javax.inject.Singleton

@Module
class MvpModule {

    @Provides
    @Singleton
    fun provideCurrenciesPresenter(): CurrenciesPresenter = CurrenciesPresenter()

    @Provides
    @Singleton
    fun provideLatestChartPresenter(): LatestChartPresenter = LatestChartPresenter()
}