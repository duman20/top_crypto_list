package kz.dev.top_crypto.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kz.dev.top_crypto.R
import kz.dev.top_crypto.databinding.RecyclerViewItemBinding

class CurrenciesAdapter : BaseAdapter<CurrenciesAdapter.CurrencyViewHolder>() {


    //создает ViewHolder и инициализирует views для списка
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val binding = RecyclerViewItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CurrencyViewHolder(binding)
    }

    //реализация вьюхолдера
    class CurrencyViewHolder(val itemBinding: RecyclerViewItemBinding) : BaseAdapter.BaseViewHolder(itemBinding.root) {


        init {
            //слушатель клика по элементам списка
            itemView.setOnClickListener {

            }
        }

        //привязываем элементы представления списка к RecyclerView и заполняем данными
        override fun bind(item: Any) {
            let {
                item as Currency
                Glide.with(view.context).load(item.image).into(itemBinding.ivCurrencyIcon)
                itemBinding.tvCurrencySym.text = item.symbol
                itemBinding.tvCurrencyName.text = item.name
                itemBinding.tvCurrencyMarketCap.text = item.marketCap
                itemBinding.tvCurrencyPrice.text = item.price.toString()

            }


        }
    }

    //класс данных для элемента списка
    data class Currency(
        val id: String,
        val symbol: String,
        val name: String,
        val image: String,
        val price: Float,
        val marketCap: String,
        val marketCapRank: Int,
        val totalVolume: Float,
        val priceChangePercentage24h: Float,
        val marketCapChangePercentage24h: Float,
        val circulatingSupply: Double,
        val totalSupply: Float,
        val ath: Float,
        val athChangePercentage: Float
    )
}